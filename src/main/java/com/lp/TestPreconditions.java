package com.lp;

import com.google.common.base.Preconditions;

/**
 * @author LiuPeng
 * @date 2017/9/11 17:52
 */

public class TestPreconditions {
    public static void main(String[] args) {
        TestPreconditions testPreconditions = new TestPreconditions();
        testPreconditions.listCheck();
    }

    public void listCheck() {
        // 检查boolean是否为true，用来检查传递给方法的参数。IllegalArgumentException
        Preconditions.checkArgument(1 >= 0, "Argument was %s but expected nonnegative", 1);

        // 检查value是否为null，该方法直接返回value，因此可以内嵌使用checkNotNull。NullPointerException
        Preconditions.checkNotNull(1);

        // 用来检查对象的某些状态。IllegalStateException
        Preconditions.checkState(true, "Status was %s but expected nonnegative", true);

        // 检查index作为位置值对某个列表、字符串或数组是否有效。index>=0 && index<=size *。IndexOutOfBoundsException
        Preconditions.checkPositionIndex(0, 12, "xxx");
        Preconditions.checkPositionIndexes(0, 3, 12);
        Preconditions.checkElementIndex(0, 12, "xxx");
    }
}
