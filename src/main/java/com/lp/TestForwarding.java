package com.lp;

import com.google.common.collect.ForwardingList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author peng.liu
 * @date 2017/10/13 16:45
 */

public class TestForwarding {

    /**
     * Forwarding抽象类以简化装饰者模式的使用
     * 
     * @param args
     */
    public static void main(String[] args) {
        TestForwarding testForwarding = new TestForwarding();
        testForwarding.doTest();
    }

    public void doTest() {
        List<String> list = new ArrayList<String>();
        list.add("123");
        list.add("345");
        list.add("678");
        list = new AddLoggingList<String>(list);
        list.add(0, "1234");
    }

    class AddLoggingList<E> extends ForwardingList<E> {

        final List<E> delegate; // backing list

        AddLoggingList(List<E> delegate) {
            this.delegate = delegate;
        }

        @Override
        protected List<E> delegate() {
            return delegate;
        }

        @Override
        public void add(int index, E elem) {
            System.out.println(index);
            System.out.println(elem.toString());
            super.add(index, elem);
        }

        @Override
        public boolean add(E elem) {
            return standardAdd(elem); // 用add(int, E)实现
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            return standardAddAll(c); // 用add实现
        }
    }

}
