package com.lp;

import com.google.common.collect.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.util.stream.Collectors.toList;

/**
 * @author peng.liu
 * @date 2017/10/13 13:46
 */

public class TestMultiCollect {

    // Map 对应的Multiset 是否支持null元素
    // HashMap HashMultiset 是（支持null元素）
    // TreeMap TreeMultiset 是（如果comparator支持的话）
    // LinkedHashMap LinkedHashMultiset 是
    // ConcurrentHashMap ConcurrentHashMultiset 否
    // ImmutableMap ImmutableMultiset 否
    public static void main(String[] args) {
        TestMultiCollect testMultiCollect = new TestMultiCollect();
        // testMultiCollect.listMultiset();
        // testMultiCollect.listMultimap();
        // testMultiCollect.listBiMap();
        testMultiCollect.listTable();
        // testMultiCollect.listRangeSet();
        // testMultiCollect.listRangeMap();
    }

    public void listMultiset() {
        // 统计一个词在文档中出现了多少次
        Multiset<Integer> multiset = HashMultiset.create();
        SortedMultiset<Integer> sortedMultiset = TreeMultiset.create();
        List<Integer> list = Lists.newArrayList();
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            Integer temp = random.nextInt(10);
            multiset.add(temp);
            sortedMultiset.add(temp);
        }
        // size()返回所有元素的总个数（包括重复的元素）
        // iterator()返回一个迭代器，包含Multiset的所有元素（包括重复的元素）
        System.out.println(multiset.size());

        // count(Object)返回给定元素的计数
        System.out.println(multiset.count(1));

        // entrySet()返回Set<Multiset.Entry<E>>，和Map的entrySet类似
        System.out.println(multiset.entrySet());

        // elementSet()返回所有不重复元素的Set<E>，和Map的keySet()类似
        System.out.println(multiset.elementSet());

        // 来统计0<=x<4之间的随机数之和
        // 统计你的站点中延迟在100毫秒以内的访问
        System.out.println(sortedMultiset.subMultiset(0, BoundType.CLOSED, 4, BoundType.OPEN).size());
    }

    /**
     * Multimap.get(key)以集合形式返回键所对应的值视图，即使没有任何对应的值，也会返回空集合。ListMultimap.get(key)返回List，SetMultimap.
     * get(key)返回Set。
     */
    // 实现 键行为类似 值行为类似
    // ArrayListMultimap HashMap ArrayList
    // HashMultimap HashMap HashSet
    // LinkedListMultimap* LinkedHashMap* LinkedList*
    // LinkedHashMultimap** LinkedHashMap LinkedHashMap
    // TreeMultimap TreeMap TreeSet
    // ImmutableListMultimap ImmutableMap ImmutableList
    // ImmutableSetMultimap ImmutableMap ImmutableSet
    public void listMultimap() {
        Multimap<String, Integer> listMultimap = ArrayListMultimap.create();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            Integer temp = random.nextInt(10);
            if (temp > 5) {
                listMultimap.put(">5", temp);
            } else {
                listMultimap.put("<=5", temp);
            }
        }
        System.out.println(listMultimap);
    }

    // 键–值实现 值–键实现 对应的BiMap实现
    // HashMap HashMap HashBiMap
    // ImmutableMap ImmutableMap ImmutableBiMap
    // EnumMap EnumMap EnumBiMap
    // EnumMap HashMap EnumHashBiMap
    public void listBiMap() {
        Map<String, Integer> nameToId = Maps.newHashMap();
        Map<Integer, String> idToName = Maps.newHashMap();
        nameToId.put("Bob", 42);
        idToName.put(42, "Bob");
        System.out.println(nameToId.get("Bob"));
        System.out.println(idToName.get(42));

        BiMap<String, Integer> userId = HashBiMap.create();
        userId.put("Bob", 42);

        // 如果你想把键映射到已经存在的值，会抛出IllegalArgumentException异常。如果对特定值，你想要强制替换它的键，请使用 BiMap.forcePut(key,
        // value)。
        userId.forcePut("Tom", 42);
        System.out.println(userId.get("Tom"));

        // 可以用 inverse()反转BiMap<K, V>的键值映射
        System.out.println(userId.inverse().get(42));
    }

    /**
     * 支持所有类型的键：”行”和”列”。Table提供多种视图
     */
    // HashBasedTable：本质上用HashMap<R, HashMap<C, V>>实现；
    // TreeBasedTable：本质上用TreeMap<R, TreeMap<C,V>>实现；
    // ImmutableTable：本质上用ImmutableMap<R, ImmutableMap<C, V>>实现；注：ImmutableTable对稀疏或密集的数据集都有优化。
    // ArrayTable：要求在构造时就指定行和列的大小，本质上由一个二维数组实现，以提升访问速度和密集Table的内存利用率
    public void listTable() {
        List<Map<String, String>> maps = ImmutableList.of(ImmutableMap.of("key", "1", "value", "a"),
                ImmutableMap.of("key", "1", "value", "b"), ImmutableMap.of("key", "2", "value", "c"));

        Table table = maps.stream().collect(() -> HashBasedTable.create(), (t, i) -> t.put(i.get("key"), i.get("value"), i),
                (t1, t2) -> t1.putAll(t2));
        System.out.println(table);
        Integer v1 = 1;
        Integer v2 = 2;
        Integer v3 = 3;
        Table<Integer, Integer, Integer> weightedGraph = HashBasedTable.create();
        weightedGraph.put(v1, v2, 4);
        weightedGraph.put(v1, v3, 20);
        weightedGraph.put(v2, v3, 5);

        System.out.println(weightedGraph.row(0).values());
        System.out.println(weightedGraph.row(1).values());
        System.out.println(weightedGraph.column(0).values());
        System.out.println(weightedGraph.column(2));
        weightedGraph.clear();
        Collection<Integer> tempList = weightedGraph.row(0).values();
        if (tempList != null) {
            System.out.println(tempList.stream().collect(toList()));
        }
        // 行Map
        System.out.println(weightedGraph.row(v1).values());
        System.out.println(weightedGraph.rowMap());

        // 列Map
        System.out.println(weightedGraph.column(v2));
        System.out.println(weightedGraph.columnMap());

        // key
        System.out.println(weightedGraph.cellSet());
    }

    /**
     * ClassToInstanceMap是一种特殊的Map：它的键是类型，而值是符合键所指类型的对象。
     */
    public void listClassToInstanceMap() {
        ClassToInstanceMap<Number> numberDefaults = MutableClassToInstanceMap.create();
        numberDefaults.putInstance(Integer.class, Integer.valueOf(0));

    }

    /**
     * RangeSet描述了一组不相连的、非空的区间。当把一个区间添加到可变的RangeSet时，所有相连的区间会被合并，空区间会被忽略。例
     */
    public void listRangeSet() {
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        rangeSet.add(Range.closed(1, 10)); // {[1,10]}
        rangeSet.add(Range.closedOpen(11, 15));// 不相连区间:{[1,10], [11,15)}
        rangeSet.add(Range.closedOpen(15, 20)); // 相连区间; {[1,10], [11,20)}
        rangeSet.add(Range.openClosed(0, 0)); // 空区间; {[1,10], [11,20)}
        rangeSet.remove(Range.open(5, 10)); // 分割[1, 10]; {[1,5], [10,10], [11,20)}

        // contains(C)：RangeSet最基本的操作，判断RangeSet中是否有任何区间包含给定元素。
        // rangeContaining(C)：返回包含给定元素的区间；若没有这样的区间，则返回null。
        // encloses(Range<C>)：简单明了，判断RangeSet中是否有任何区间包括给定区间。
        // span()：返回包括RangeSet中所有区间的最小区间。
        System.out.println(rangeSet);
    }

    /**
     * RangeMap描述了”不相交的、非空的区间”到特定值的映射。和RangeSet不同，RangeMap不会合并相邻的映射，即便相邻的区间映射到相同的值。
     */
    public void listRangeMap() {
        RangeMap<Integer, String> rangeMap = TreeRangeMap.create();
        rangeMap.put(Range.closed(1, 10), "foo"); // {[1,10] => "foo"}
        rangeMap.put(Range.open(3, 6), "bar"); // {[1,3] => "foo", (3,6) => "bar", [6,10] => "foo"}
        rangeMap.put(Range.open(10, 20), "foo"); // {[1,3] => "foo", (3,6) => "bar", [6,10] =>
                                                 // "foo", (10,20) => "foo"}
        rangeMap.remove(Range.closed(5, 11)); // {[1,3] => "foo", (3,5) => "bar", (11,20) => "foo"}

        // asMapOfRanges()：用Map<Range<K>, V>表现RangeMap。这可以用来遍历RangeMap。
        // subRangeMap(Range<K>)：用RangeMap类型返回RangeMap与给定Range的交集视图。这扩展了传统的headMap、subMap和tailMap操作。
        System.out.println(rangeMap);
    }
}
