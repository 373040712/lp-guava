package com.lp;

import com.google.common.collect.*;

import java.util.List;
import java.util.Map;

/**
 * @author peng.liu
 * @date 2017/10/13 15:50
 */

public class TestCollectUtil {

    // 集合接口 属于JDK还是Guava 对应的Guava工具类
    // Collection JDK Collections2：不要和java.util.Collections混淆
    // List JDK Lists
    // Set JDK Sets
    // SortedSet JDK Sets
    // Map JDK Maps
    // SortedMap JDK Maps
    // Queue JDK Queues
    // Multiset Guava Multisets
    // Multimap Guava Multimaps
    // BiMap Guava Maps
    // Table Guava Tables
    public static void main(String[] args) {
        TestCollectUtil testCollectUtil = new TestCollectUtil();
        // testCollectUtil.listIterables();
        testCollectUtil.listMaps();
    }

    public void doFactory() {
        List<String> list = Lists.newArrayList();
        List<String> list1 = Lists.newArrayList("alpha", "beta", "gamma");
        List<String> list2 = Lists.newArrayListWithCapacity(100);
        List<String> list3 = Lists.newArrayListWithExpectedSize(100);
    }

    /**
     * Iterables使用FluentIterable类进行了补充，它包装了一个Iterable实例，并对许多操作提供了”fluent”（链式调用）语法
     */
    public void listIterables() {
        List<String> list = Lists.newArrayList("alpha", "beta", "gamma", "gamma", "gamma", "gamma");
        List<String> list0 = Lists.newArrayList("beta", "gamma", "gamma", "tom");

        // 返回对象在iterable中出现的次数
        System.out.println(Iterables.frequency(list, "gamma"));

        // 返回给定List的反转视图。注: 如果List是不可变的，考虑改用ImmutableList.reverse()。
        System.out.println(Lists.reverse(list));

        // 集合并集
        System.out.println(Sets.union(Sets.newHashSet(list0), Sets.newHashSet(list)));

        // 集合交集
        System.out.println(Sets.intersection(Sets.newHashSet(list0), Sets.newHashSet(list)));

        // A对B相差元素
        System.out.println(Sets.difference(Sets.newHashSet(list0), Sets.newHashSet(list)));

        // A对B，B对A的相差元素
        System.out.println(Sets.symmetricDifference(Sets.newHashSet(list0), Sets.newHashSet(list)));
    }

    public void listMaps() {
        Map<String, Integer> left = ImmutableMap.of("a", 1, "b", 2);
        Map<String, Integer> right = ImmutableMap.of("b", 2, "c", 3);
        MapDifference<String, Integer> diff = Maps.difference(left, right);
        System.out.println(diff.entriesInCommon()); // {"b" => 2}
        System.out.println(diff.entriesOnlyOnLeft()); // {"a" => 1}
        System.out.println(diff.entriesOnlyOnRight()); // {"c" => 3}
    }

}
