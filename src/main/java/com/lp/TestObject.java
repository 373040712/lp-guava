package com.lp;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import com.lp.pojo.EmployeeInfo;

/**
 * @author LiuPeng
 * @date 2017/9/11 18:03
 */

public class TestObject {

    public static void main(String[] args) {
        TestObject testObject = new TestObject();
        testObject.doHashCode();
    }

    public void doEqual() {
        // JDK7引入的Objects类提供了一样的方法Objects.equals。
        Objects.equal("a", "a"); // returns true
        Objects.equal(null, "a"); // returns false
        Objects.equal("a", null); // returns false
        Objects.equal(null, null); // returns true
    }

    public void doHashCode() {
        // JDK7引入的Objects类提供了一样的方法Objects.hash(Object...)
        EmployeeInfo employeeInfo = new EmployeeInfo();
        System.out.println(Objects.hashCode(employeeInfo.getUserId(), employeeInfo.getWorkNo()));
    }

    class Foo implements Comparable<Foo> {
        private int id;
        private String name;
        private Integer age;

        // ComparisonChain执行一种懒比较：它执行比较操作直至发现非零的结果，在那之后的比较输入将被忽略。
        public int compareTo(Foo that) {
            return ComparisonChain.start().compare(this.id, that.id, Ordering.natural().nullsLast()).result();
        }
    }
}
