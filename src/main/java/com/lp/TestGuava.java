package com.lp;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017-8-14 0014.
 */
public class TestGuava {

    public static void main(String[] args) {
        TestGuava testGuava = new TestGuava();
        testGuava.testImmutable();
        testGuava.testMultiset();
    }

    public void testImmutable() {
        ArrayList<String> stringArrayList = Lists.newArrayList("wo", "bu", "ke", "bian");
        ImmutableList<String> immutableList = ImmutableList.copyOf(stringArrayList);
        // 尝试add： java.lang.UnsupportedOperationException
        // immutableList.add("!!!");


        // 尝试修改原集合：immutableList不变，还是 [wo, bu, ke, bian]
        stringArrayList.add("!!!");
        System.out.println(immutableList);
    }

    public void testMultiset() {
        Multiset<Integer> multiset = HashMultiset.create();
        multiset.add(1);
        multiset.add(2);
        multiset.add(3);
        multiset.add(3);
        multiset.add(3);
        System.out.println(multiset.count(3));
        System.out.println(multiset.size());
        System.out.println(multiset.elementSet());
    }

}
