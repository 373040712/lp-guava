package com.lp;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

/**
 * @author peng.liu
 * @date 2017/11/2 18:06
 */

public class TestString {

    public static void main(String[] args) {
        System.out.println(Joiner.on(",").appendTo(new StringBuilder("123"), Lists.newArrayList("a","b","c")).toString());
        TestString testString = new TestString();
        testString.testCharMatcher();
//        testString.testSplitter();
//        testString.testJoiner();
    }


    // 方法 描述
    // collapseFrom(CharSequence, char) 把每组连续的匹配字符替换为特定字符。如WHITESPACE.collapseFrom(string, ‘
    // ‘)把字符串中的连续空白字符替换为单个空格。
    // matchesAllOf(CharSequence) 测试是否字符序列中的所有字符都匹配。
    // removeFrom(CharSequence) 从字符序列中移除所有匹配字符。
    // retainFrom(CharSequence) 在字符序列中保留匹配字符，移除其他字符。
    // trimFrom(CharSequence) 移除字符序列的前导匹配字符和尾部匹配字符。
    // replaceFrom(CharSequence, CharSequence) 用特定字符序列替代匹配字符。
    private void testCharMatcher() {
        // 移除control字符
        String string = "   12as2\t34\t5AD    B<J?ad  %*CHas   ";
        String temp=CharMatcher.anyOf("as").removeFrom(string);
        System.out.println(temp);

        String noControl = CharMatcher.javaIsoControl().removeFrom(string);
        System.out.println(noControl);

        // 只保留数字字符
        String theDigits = CharMatcher.digit().retainFrom(string);
        System.out.println(theDigits);

        // 去除两端的空格，并把中间的连续空格替换成单个空格
        String spaced = CharMatcher.whitespace().trimAndCollapseFrom(string, ' ');
        System.out.println(spaced);

        // 用*号替换所有数字
        String noDigits = CharMatcher.javaDigit().replaceFrom(string, "*");
        System.out.println(noDigits);

        // 只保留数字和小写字母
        String lowerAndDigit = CharMatcher.javaDigit().or(CharMatcher.javaLowerCase()).retainFrom(string);
        System.out.println(lowerAndDigit);

        // 只保留范围内字符
        String azStr = CharMatcher.inRange('a', 'd').retainFrom(string);
        System.out.println(azStr);

    }

    private void testSplitter() {
        System.out.println(Splitter.on(',').trimResults().omitEmptyStrings().split("foo,bar,,   qux"));
    }

    private void testJoiner() {
        Joiner joiner = Joiner.on("; ").skipNulls();
        System.out.println(joiner.join("Harry", null, "Ron", "Hermione"));
    }
}
