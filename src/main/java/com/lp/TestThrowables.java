package com.lp;

import com.google.common.base.Throwables;

import java.io.FileNotFoundException;

/**
 * @author LiuPeng
 * @date 2017/9/12 16:41
 */

public class TestThrowables {

    public static void main(String[] args) {
        TestThrowables testThrowables = new TestThrowables();
        try {
            testThrowables.listMethod();
        } catch (Exception e) {
            Throwables.getRootCause(e);
            Throwables.getCausalChain(e);
            Throwables.getStackTraceAsString(e);
        }

    }

    public void listMethod() throws Exception {
        try {
            throw new FileNotFoundException();
        } catch (Throwable t) {
            // Throwable类型为X, Error或RuntimeException才抛出
            Throwables.propagateIfPossible(t, Exception.class);

            // 完全等价于throw new RuntimeException(t)
            // Throwables.propagate不是为了鼓励开发者忽略IOException这样的异常。
            Throwables.propagate(t);

        }

    }
}
