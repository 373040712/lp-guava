package com.lp.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author LiuPeng
 * @date 2017/9/11 18:10
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeInfo implements Serializable {

    private static final long serialVersionUID = -7122758071956979692L;

    private long employeeId; // 员工Id
    private String workNo; // 工号
    private String name; // 姓名
    private String status; // 在职状态
    private String positionName; // 岗位
    private long organizationId; // 机构Id
    private String organizationShortName; // 所属机构（大区/部门）
    private String organizationName; // 所属机构（大区/部门）
    private boolean enabled; // 账号状态
    private long userId; // 用户id
    private String establishmentAddress; // 编制地
    private String workAddress; // 工作地
    private String mobile;// 电话
    private String username;// 用户账号
    private double lat;// 最新更新维度
    private double lng;// 最新更新经度
}
